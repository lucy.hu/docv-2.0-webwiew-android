package com.trulioo.webwiewandroid

import android.Manifest.permission.CAMERA
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.webkit.PermissionRequest
import android.webkit.PermissionRequest.RESOURCE_VIDEO_CAPTURE
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {

    private val permission = arrayOf(CAMERA)
    private val requestCode = 1
    //private val webViewURL = "https://trulioo-android-webview.ngrok.io"
    private val webViewURL = "https://ENTER_URL_HERE.com"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        println("permissions granted: " + isPermissionGranted())
        if (!isPermissionGranted()) {
            askPermissions()
        }

        val webView: WebView = findViewById(R.id.webView)
        val refreshButton: Button = findViewById(R.id.refreshButton)

        webView.webChromeClient = object : WebChromeClient() {
            // Passes permissions from app level to WebView
            override fun onPermissionRequest(request: PermissionRequest) {
                println("WebView requesting permission")
                val requestedResource = request.resources
                for (resource in requestedResource) {
                    println("Requesting: $resource")
                    if (resource.equals(RESOURCE_VIDEO_CAPTURE)) {
                        request.grant(arrayOf(resource))
                    }
                }
            }
        }

        webView.apply {
            // enables the javascript settings, but can also allow xss vulnerabilities
            settings.javaScriptEnabled = true
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.domStorageEnabled = true
            settings.allowContentAccess = true
            settings.mediaPlaybackRequiresUserGesture = false
        }

        refreshButton.setOnClickListener {
            webView.reload()
        }

        // Load the URL
        if (savedInstanceState == null) {
            webView.loadUrl(webViewURL)
        }
    }

    private fun askPermissions() {
        println("asking permissions")
        ActivityCompat.requestPermissions(this, permission, requestCode)
    }

    // App level ask for permissions
    private fun isPermissionGranted(): Boolean {
        permission.forEach {
            if (ActivityCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED)
                return false
        }
        return true
    }
}